<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once("connect.php");

global $conn;
header('Content-type: application/json');

$uploadOk = 0;

try {	
$id = $_SESSION['user'];
/*$form = $_POST['form'];
$file = $_POST['file'];
$content = $_POST['content'];
var_dump($id, $file, $content);*/
	if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'];
        exit();
    }
    
	$sql = "select count(user_id) from posts where user_id=$id;";
	$res = $conn->query($sql) or die($conn->error);

	$res = $res->fetch_assoc();
	$n = $res['count(user_id)'] + 1;

	$sql = "insert into posts(user_id, post_number, created, likes) 		values ($id, $n, now(), 0);";
	$res = $conn->query($sql) or die($conn->error);

	$pid = $conn->insert_id;

	$target_dir = "uploads/";
	$target_file = $target_dir.$pid."_".$_FILES['file']['name'];

	if(move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
		$sql = "insert into post_content values ($pid, $target_file, $content);";
		$res = $conn->query($sql) or die($conn->error);
		$uploadOk = 1;
	} else {
		echo "{\"op\":\"bleh\"}";
	}

	$resp = "{\"Success\": \"True\",";
	$resp .= "\"File\" : ".$file.",";
	$resp .= "\"uploadOk\" : ".$uploadOk."}";
	echo $resp;
}
catch (Exception $e) {
	$resp = "{\"Success\": \"False\",";
	$resp .= "\"Error\" : ".$e.",";
	$resp .= "\"uploadOk\" : ".$uploadOk."}";
	echo $resp;
}

?>