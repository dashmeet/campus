drop table blog_body;
drop table blogs;
drop table post_content;
drop table comments;
drop table saved;
drop table posts;
drop table news;
drop table upcoming;
drop table follow;
drop table user;
drop table page;

/*user(user_id, username, password, email, dob, branch, year, bio, picture, status)*/
create table user (
	user_id integer not null auto_increment primary key,
	username varchar(24) not null,
	password varchar(224) not null,
	email varchar(32),
	dob varchar(20),
	branch varchar(24),
	year varchar(8),
	bio varchar(128),
	picture blob,
	status varchar(8),
	unique key (username, email)
);

/*page(page_id, pagename, password, email, bio, picture, status)*/
create table page (
	page_id integer not null auto_increment primary key,
	pagename varchar(24) not null,
	password blob not null,
	email varchar(32),
	bio varchar(128),
	picture blob,
	status varchar(8),
	unique key (pagename, email)
);

/*blogs(user_id, blog_number, title, body, date, likes, shares)*/
create table blogs (
	blog_id integer not null auto_increment primary key,
	user_id integer not null,
	blog_number integer not null,
	title varchar(64),
	created datetime,
	likes integer,
	shares integer,
	unique key (user_id, blog_number),
	foreign key (user_id) references user (user_id)
);

create table blog_body (
	blog_id integer not null,
	bbody text,
	foreign key (blog_id) references blogs (blog_id)
);

/*follow(follower_id, follows_id)*/
create table follow (
	follower_id integer not null,
	follows_id integer not null,
	primary key (follower_id, follows_id),
	foreign key (follower_id) references user (user_id),
	foreign key (follows_id) references user (user_id)
);

/*posts(post_id, user_id, post_number, date, time, likes)*/
create table posts (
	post_id integer not null auto_increment primary key,
	user_id integer not null,
	post_number integer not null default 0,
	created datetime,
	likes integer,
	foreign key (user_id) references user (user_id),
);

/*post_media(post_id, blob)*/
create table post_content (
	post_id integer not null,
	filename varchar(100),
	description varchar(400),
	foreign key (post_id) references posts (post_id)
			on delete cascade
);

/*comments(post_id, user_id, comment)*/
create table comments (
	post_id integer not null,
	user_id integer not null,
	comment varchar(128),
	foreign key (post_id) references posts (post_id),
	foreign key (user_id) references user (user_id)	
);

/*saved(user_id, post_id)*/
create table saved (
	user_id integer not null,
	post_id integer not null,
	primary key (user_id, post_id),
	foreign key (post_id) references posts (post_id),
	foreign key (user_id) references user (user_id)	
);

/*news(news_id, title, content)*/
create table news (
	news_id integer not null primary key,
	title varchar(64),
	content text
);

/*upcoming(event_id, title, content, from, to)*/
create table upcoming (
	event_id integer not null primary key,
	title varchar(64),
	content text,
	start date,
	end date
);