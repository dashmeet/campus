var options = {};

//SIDENAV
var elem = document.querySelector('.sidenav');
var instance = M.Sidenav.init(elem, options);

//MOBILE FOOTER
var elem = document.querySelector('.collapsible');
var instance = M.Collapsible.init(elem, options);

//DATEPICKER
var elem = document.querySelector('.datepicker');
var instance = M.Datepicker.init(elem, {
	yearRange: 30
});

//SELECT
var elem = document.querySelector('select');
var instance = M.FormSelect.init(elem, options);

//MODAL
var elem = document.querySelector('.modal');
var instance = M.Modal.init(elem, options);

//UPLOAD LIMIT CONTENT
$(document).ready(function() {
    $('input#post-content').characterCounter();
});
